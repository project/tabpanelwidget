# TabPanelWidget

My (government) employer had a need for responsive tabs that would collapse
to accordions at narrower widths, and also met accessibility requirements.
[TabPanelWidget](https://tabpanelwidget.com) met both needs. These modules
make the library available for Drupal display purposes:

* **TabPanelWidget** defines the library and provides a class for setting
  options, generating markup, and attaching the library.

* **TabPanelWidget Quick Tabs** provides a TabRenderer plugin for Quick Tabs.

* **TabPanelWidget Views** provides a style plugin for Views.

## Installing the library with Composer

Until I can get composer.libraries.json working with the composer merge plugin,
add the following to your composer.json repositories list:

    {
      "type": "package",
      "package": {
        "name": "tabpanelwidget/tabpanelwidget",
        "version": "1.4.0",
        "type": "drupal-library",
        "dist": {
          "url": "https://github.com/tabpanelwidget/tabpanelwidget/releases/download/1.4.0/tabpanelwidget-1.4.0.zip",
          "type": "zip"
        }
      }
    },

...and use:

    composer require tabpanelwidget/tabpanelwidget:^1.2

...to add it to your project.
