<?php

namespace Drupal\tabpanelwidget;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A Tpw object for configuring and rendering a tabset.
 */
class Tpw {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Options from this instance, initially populated from the module defaults.
   *
   * @var array
   */
  private $options;

  /**
   * Content of the tabset.
   *
   * @var array
   */
  private $items;

  /**
   * Client constructor.
   */
  public function __construct() {
    $this->configFactory = \Drupal::service('config.factory');
    $this->options = $this->configFactory->getEditable('tabpanelwidget.settings');
    $this->items = [];
  }

  /**
   * Method description.
   */
  public function build() {

    $build = [
      '#attributes' => [
        'class' => [
          'tpw-wrapper',
        ],
      ],
      'widget' => [
        '#theme' => 'tpw',
        '#attributes' => [
          'class' => [
            'tpw-widget',
          ],
        ],
        '#items' => [],
      ],
    ];

    // Force tabs or accordion if one of those options is set.
    if ($this->options->get('behavior') == 'tabpanel') {
      $build['widget']['#attributes']['class'][] = 'tpw-tabpanel';
    }
    elseif ($this->options->get('behavior') == 'accordion') {
      $build['widget']['#attributes']['class'][] = 'tpw-accordion';
    }

    // If we're *not* forcing accordion mode, we need to set tab options.
    if ($this->options->get('behavior') != 'accordion') {
      switch ($this->options->get('tab_style')) {
        case 'fancy':
          $build['widget']['#attributes']['class'][] = 'tpw-fancy';
          break;

        case 'pills':
          $build['widget']['#attributes']['class'][] = 'tpw-pills';
          break;

        case 'bar':
          $build['widget']['#attributes']['class'][] = 'tpw-bar';
          break;
      }
      if ($this->options->get('tab_options')['centered']) {
        $build['widget']['#attributes']['class'][] = 'tpw-centered';
      }
      if ($this->options->get('tab_options')['rounded']) {
        $build['widget']['#attributes']['class'][] = 'tpw-rounded';
      }
    }

    // If we're *not* forcing tabs mode, we need to set accordion options.
    if ($this->options->get('behavior') != 'tabpanel') {
      if ($this->options->get('accordion_options')['disconnected']) {
        $build['widget']['#attributes']['class'][] = 'tpw-disconnected';
      }
      if ($this->options->get('accordion_options')['icons_at_the_end']) {
        $build['widget']['#attributes']['class'][] = 'tpw-icons-at-the-end';
      }
      if ($this->options->get('accordion_options')['chevrons_east_south']) {
        $build['widget']['#attributes']['class'][] = 'tpw-chevrons-east-south';
      }
      if ($this->options->get('accordion_options')['plus_minus']) {
        $build['widget']['#attributes']['class'][] = 'tpw-plus-minus';
      }
      if ($this->options->get('accordion_options')['animate']) {
        $build['widget']['#attributes']['class'][] = 'tpw-animate';
      }
    }

    // Add the items.
    foreach ($this->getItems() as $item) {
      $item_build = $item->build($this->options->get('elements'));
      $build['widget']['#items'][] = [
        '#markup' => \Drupal::service('renderer')->render($item_build),
      ];
      // $build['#items'][] = $item->build($this->options->get('elements'));
    }

    // Attach the libraries.
    if ($this->options->get('polyfill')) {
      $build['widget']['#attached']['library'][] = 'tabpanelwidget/tabpanelwidget.polyfill';
    }
    $build['widget']['#attached']['library'][] = 'tabpanelwidget/tabpanelwidget.default';

    return $build;
  }

  /**
   * Get options.
   */
  public function getOptions() {
    return $this->options->get();
  }

  /**
   * Set elements.
   */
  public function setElements(string $elements) {
    $this->options->set('elements', $elements);
  }

  /**
   * Set behavior.
   */
  public function setBehavior(string $behavior) {
    $this->options->set('behavior', $behavior);
  }

  /**
   * Set tab style.
   */
  public function setTabStyle(string $tab_style) {
    $this->options->set('tab_style', $tab_style);
  }

  /**
   * Set tab options.
   */
  public function setTabOptions(array $tab_options) {
    $this->options->set('tab_options', $this->normalizeOptions($tab_options));
  }

  /**
   * Set tab options.
   */
  public function setAccordionOptions(array $accordion_options) {
    $this->options->set('accordion_options', $this->normalizeOptions($accordion_options));
  }

  /**
   * Add a new item.
   */
  public function addItem(string $title, array $content, bool $default = FALSE) {
    $item = new TpwItem();
    $item->setTitle($title);
    $item->setContent($content);
    $item->setDefault($default);
    $this->items[] = $item;
  }

  /**
   * Get all items.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Reset all values.
   *
   * It seems like this shouldn't be necessary, but it looks as if when Views
   * uses the same style plugin multiple times on one page, it reuses it rather
   * than creating a new instance? Either that or something is very wrong with
   * my stuff.
   */
  public function reset() {
    $this->options = $this->configFactory->getEditable('tabpanelwidget.settings');
    $this->items = [];
  }

  /**
   * Normalize input values so that expected booleans are actually boolean.
   */
  private function normalizeOptions($options_input) {
    $options_output = [];
    foreach ($options_input as $key => $value) {
      $options_output[$key] = !empty($value) ? TRUE : FALSE;
    }
    return $options_output;
  }

}
