<?php

namespace Drupal\tabpanelwidget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure TabPanelWidget settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tabpanelwidget_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tabpanelwidget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('tabpanelwidget.settings');

    $form['instructions'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<p>These will the the default settings used for new TabPanelWidget displays. Individual displays may override these settings.</p>'),
    ];

    $form['main_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Main settings'),
      '#open' => TRUE,
    ];

    $form['main_settings']['elements'] = [
      '#type' => 'select',
      '#title' => $this->t('Elements'),
      '#options' => [
        'h2' => $this->t('H2'),
        'h3' => $this->t('H3'),
        'h4' => $this->t('H4'),
        'h5' => $this->t('H5'),
      ],
      '#default_value' => !empty($settings->get('elements')) ? $settings->get('elements') : 'h2',
      '#description' => $this->t('<p>The element used for the headers and tabs. H2 is probably usually appropriate if the tabset is in the main body of a page and not under other headers. You may wish to change this if the tabset will be nested under other headers.</p>'),
    ];

    $form['main_settings']['behavior'] = [
      '#type' => 'select',
      '#title' => $this->t('Behavior'),
      '#options' => [
        'responsive' => $this->t('Responsive'),
        'tabpanel' => $this->t('Always display as tabs'),
        'accordion' => $this->t('Always display as accordion'),
      ],
      '#default_value' => !empty($settings->get('behavior')) ? $settings->get('behavior') : 'responsive',
      '#description' => $this->t('<p>Responsive tabs will change to accordions if the tabs cannot all fit on a single row.</p>'),
    ];

    $form['main_settings']['polyfill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Serve polyfill'),
      '#default_value' => !empty($settings->get('polyfill')) ? $settings->get('polyfill') : FALSE,
      '#description' => $this->t('<p>This option will enable support for IE10 and 11, as well as several other older browsers. This is a global setting that will affect all TabPanelWidgets on your site.</p>'),
    ];

    $form['tab_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Tab settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="behavior"]' => [
            ['value' => 'responsive'],
            ['value' => 'tabpanel'],
          ],
        ],
      ],
    ];

    $form['tab_settings']['tab_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Tab style'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'fancy' => $this->t('Fancy'),
        'pills' => $this->t('Pills'),
        'bar' => $this->t('Bar'),
      ],
      '#default_value' => !empty($settings->get('tab_style')) ? $settings->get('tab_style') : 'standard',
      '#description' => $this->t('<p><b>Standard:</b> Foreground tab is styled, background tabs are plain labels.<br><b>Fancy:</b> All tabs are styled.<br><b>Pills:</b> Tabs are displayed as a row of "pills."<br><b>Bar:</b> Tabs are plain labels with a bar highlighting the selected tab.</p>'),
    ];

    $form['tab_settings']['tab_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Tab options'),
      '#options' => [
        'centered' => $this->t('Centered'),
        'rounded' => $this->t('Rounded'),
      ],
      '#default_value' => [],
    ];
    foreach ($settings->get('tab_options') as $key => $value) {
      if ($value) {
        $form['tab_settings']['tab_options']['#default_value'][] = $key;
      }
    }
    $form['tab_settings']['tab_options']['centered']['#description'] = $this->t('Tabs are centered rather than left-aligned.');
    $form['tab_settings']['tab_options']['rounded']['#description'] = $this->t('Tabs and borders have rounded corners. Does not apply to "Bar" style.');

    $form['accordion_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Accordion settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="behavior"]' => [
            ['value' => 'responsive'],
            ['value' => 'accordion'],
          ],
        ],
      ],
    ];

    $form['accordion_settings']['accordion_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Accordion options'),
      '#options' => [
        'disconnected' => $this->t('Disconnected'),
        'icons_at_the_end' => $this->t('Icons at the end'),
        'chevrons_east_south' => $this->t('Chevrons east/south'),
        'plus_minus' => $this->t('Replace chevrons with +/-'),
        'animate' => $this->t('Animate expand/collapse icons'),
      ],
      '#default_value' => [],
    ];
    foreach ($settings->get('accordion_options') as $key => $value) {
      if ($value) {
        $form['accordion_settings']['accordion_options']['#default_value'][] = $key;
      }
    }
    $form['accordion_settings']['accordion_options']['disconnected']['#description'] = $this->t('Creates space between the headers and styles them with rounded corners.');
    $form['accordion_settings']['accordion_options']['icons_at_the_end']['#description'] = $this->t('Display the expand/collapse icons at the end of the headers.');
    $form['accordion_settings']['accordion_options']['chevrons_east_south']['#description'] = $this->t('Override expand/collapse chevrons to point East/South instead of South/North.');
    $form['accordion_settings']['accordion_options']['plus_minus']['#description'] = $this->t('Replace expand/collapse chevrons with plus and minus signs.');
    $form['accordion_settings']['accordion_options']['animate']['#description'] = $this->t('Animate the state changes of the expand/collapse icons.');

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tab_style = $form_state->getValue('behavior') != 'accordion' ? $form_state->getValue('tab_style') : 'standard';
    $tab_options = $form_state->getValue('behavior') != 'accordion' ? $form_state->getValue('tab_options') : [];
    $accordion_options = $form_state->getValue('behavior') != 'accordion' ? $form_state->getValue('accordion_options') : [];
    $this->config('tabpanelwidget.settings')
      ->set('elements', $form_state->getValue('elements'))
      ->set('behavior', $form_state->getValue('behavior'))
      ->set('polyfill', $form_state->getValue('polyfill'))
      ->set('tab_style', $tab_style)
      ->set('tab_options', $tab_options)
      ->set('accordion_options', $accordion_options)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
