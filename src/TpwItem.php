<?php

namespace Drupal\tabpanelwidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * TpwItem -- a single title/content combination within a tab panel.
 */
class TpwItem {

  use StringTranslationTrait;

  /**
   * The title, used as the tab title or accordion header.
   *
   * @var string
   */
  protected $title;

  /**
   * The content of the tab or accordion panel.
   *
   * @var string
   */
  protected $content;

  /**
   * Whether this tab is the default.
   *
   * @var bool
   */
  protected $default;

  /**
   * Get a render array for this item.
   */
  public function build(string $element = 'h2') {
    $build = [
      '#theme' => 'tpw_item',
      '#element' => $element,
      '#attributes' => [],
      '#title' => [
        '#markup' => $this->title,
      ],
      '#content' => $this->content,
    ];
    if ($this->default) {
      $build['#attributes']['class'][] = 'tpw-selected';
    }
    return $build;
  }

  /**
   * Get the value of the title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Set the value of the title.
   *
   * @param string $title
   *   The title to be set.
   */
  public function setTitle(string $title) {
    $title = Html::decodeEntities($title);

    // Remove comments in case twig debugging is enabled
    $this->title = preg_replace('/<!--(.|\s)*?-->/', '', $title);
  }

  /**
   * Get the value of the content.
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * Set the value of the content.
   *
   * @param array $content
   *   A render array of content.
   */
  public function setContent(array $content) {
    $this->content = $content;
  }

  /**
   * Get whether this tab is the default.
   */
  public function isDefault() {
    return $this->default;
  }

  /**
   * Set default item status.
   *
   * @param bool $default
   *   Whether or not this is the default item.
   */
  public function setDefault(bool $default) {
    $this->default = $default;
  }

}
