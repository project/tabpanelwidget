<?php

namespace Drupal\tabpanelwidget_views\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tabpanelwidget\Tpw;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * TabPanelWidget style plugin.
 *
 * @ViewsStyle(
 *   id = "tabpanelwidget_views",
 *   title = @Translation("TabPanelWidget"),
 *   help = @Translation("Foo style plugin help."),
 *   theme = "views_style_tabpanelwidget_views",
 *   display_types = {"normal"}
 * )
 */
class TabPanelWidget extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * TabPanelWidget service.
   *
   * @var \Drupal\tabpanelwidget\Tpw
   */
  private $tpw;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
    // $this->tpw = $container->get('tabpanelwidget.tpw');
    // $this->tpw = \Drupal::service('tabpanelwidget.tpw');
    $this->tpw = new Tpw();
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $default_options = \Drupal::service('config.factory')->getEditable('tabpanelwidget.settings');

    $options['first_row_default']['default'] = TRUE;

    $options['tabpanelwidget_settings'] = [
      'default' => [
        'main_settings' => [
          'elements' => $default_options->get('elements'),
          'behavior' => $default_options->get('behavior'),
        ],
        'tab_settings' => [
          'tab_style' => $default_options->get('tab_style'),
          'tab_options' => $default_options->get('tab_options'),
        ],
        'accordion_settings' => [
          'accordion_options' => $default_options->get('accordion_options'),
        ],
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $default_options = \Drupal::service('config.factory')->getEditable('tabpanelwidget.settings');

    foreach ($form['grouping'] as $index => &$field) {
      if ($index == 0) {
        $field['field']['#required'] = 1;
        $field['rendered']['#default_value'] = TRUE;
        $field['rendered']['#access'] = FALSE;
        $field['rendered_strip']['#access'] = FALSE;
      }
      // Only allow 1 level of grouping.
      elseif ($index > 0) {
        unset($form['grouping'][$index]);
      }

      $current_value = $field['field']['#description']->getUntranslatedString();
      $field['field']['#description'] = $this->t("You must specify a field by which to group the records. This field will be used for the title of each tab. <strong>Tip:</strong> hide this field from display if you don't want it to appear again in the body of the tab or accordion.", ['@current_value' => $current_value]);
    }

    $form['first_row_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use first row as default item?'),
      '#default_value' => $this->options['first_row_default'],
      '#description' => $this->t('If checked, the first item in the tab or accordion will be set as default, causing it to be visible by default. Uncheck this if you would like the entire accordion to be closed on page load.'),
    ];

    $form['tabpanelwidget_settings'] = [
      '#type' => 'container',
      '#title' => $this->t('TabPanelWidget settings'),
    ];

    $form['tabpanelwidget_settings']['main_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Main settings'),
      '#open' => TRUE,
    ];

    $form['tabpanelwidget_settings']['main_settings']['elements'] = [
      '#type' => 'select',
      '#title' => $this->t('Elements'),
      '#options' => [
        'h2' => $this->t('H2'),
        'h3' => $this->t('H3'),
        'h4' => $this->t('H4'),
        'h5' => $this->t('H5'),
      ],
      '#default_value' => !empty($this->options['tabpanelwidget_settings']['main_settings']['elements']) ? $this->options['tabpanelwidget_settings']['main_settings']['elements'] : $default_options->get('elements'),
      '#description' => $this->t('<p>The element used for the headers and tabs. H3 is probably usually appropriate, though you may wish to change this if the tabset will be nested under other headers.</p>'),
    ];

    $form['tabpanelwidget_settings']['main_settings']['behavior'] = [
      '#type' => 'select',
      '#title' => $this->t('Behavior'),
      '#options' => [
        'responsive' => $this->t('Responsive'),
        'tabpanel' => $this->t('Always display as tabs'),
        'accordion' => $this->t('Always display as accordion'),
      ],
      '#default_value' => !empty($this->options['tabpanelwidget_settings']['main_settings']['behavior']) ? $this->options['tabpanelwidget_settings']['main_settings']['behavior'] : $default_options->get('behavior'),
    ];

    $form['tabpanelwidget_settings']['tab_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Tab settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="style_options[tabpanelwidget_settings][main_settings][behavior]"]' => [
            ['value' => 'responsive'],
            ['value' => 'tabpanel'],
          ],
        ],
      ],
    ];

    $form['tabpanelwidget_settings']['tab_settings']['tab_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Tab style'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'fancy' => $this->t('Fancy'),
        'pills' => $this->t('Pills'),
        'bar' => $this->t('Bar'),
      ],
      '#default_value' => !empty($this->options['tabpanelwidget_settings']['tab_settings']['tab_style']) ? $this->options['tabpanelwidget_settings']['tab_settings']['tab_style'] : $default_options->get('tab_style'),
      '#description' => $this->t('<p><b>Standard:</b> Foreground tab is styled, background tabs are plain labels.<br><b>Fancy:</b> All tabs are styled.<br><b>Pills:</b> Tabs are displayed as a row of "pills."<br><b>Bar:</b> Tabs are plain labels with a bar highlighting the selected tab.</p>'),
    ];

    $form['tabpanelwidget_settings']['tab_settings']['tab_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Tab options'),
      '#options' => [
        'centered' => $this->t('Centered'),
        'rounded' => $this->t('Rounded'),
      ],
      '#default_value' => [],
    ];
    $defaults_array = !empty($this->options['tabpanelwidget_settings']['tab_settings']['tab_options']) ? $this->options['tabpanelwidget_settings']['tab_settings']['tab_options'] : $default_options->get('tab_options');
    foreach ($defaults_array as $key => $value) {
      if ($value) {
        $form['tabpanelwidget_settings']['tab_settings']['tab_options']['#default_value'][] = $key;
      }
    }
    $form['tabpanelwidget_settings']['tab_settings']['tab_options']['centered']['#description'] = $this->t('Tabs are centered rather than left-aligned.');
    $form['tabpanelwidget_settings']['tab_settings']['tab_options']['rounded']['#description'] = $this->t('Tabs and borders have rounded corners. Does not apply to "Bar" style.');

    $form['tabpanelwidget_settings']['accordion_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Accordion settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="style_options[tabpanelwidget_settings][main_settings][behavior]"]' => [
            ['value' => 'responsive'],
            ['value' => 'accordion'],
          ],
        ],
      ],
    ];

    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Accordion options'),
      '#options' => [
        'disconnected' => $this->t('Disconnected'),
        'icons_at_the_end' => $this->t('Icons at the end'),
        'chevrons_east_south' => $this->t('Chevrons east/south'),
        'plus_minus' => $this->t('Replace chevrons with +/-'),
        'animate' => $this->t('Animate expand/collapse icons'),
      ],
      '#default_value' => [],
    ];
    $defaults_array = !empty($this->options['tabpanelwidget_settings']['accordion_settings']['accordion_options']) ? $this->options['tabpanelwidget_settings']['accordion_settings']['accordion_options'] : $default_options->get('accordion_options');
    foreach ($defaults_array as $key => $value) {
      if ($value) {
        $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['#default_value'][] = $key;
      }
    }
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['disconnected']['#description'] = $this->t('Creates space between the headers and styles them with rounded corners.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['icons_at_the_end']['#description'] = $this->t('Display the expand/collapse icons at the end of the headers.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['chevrons_east_south']['#description'] = $this->t('Override expand/collapse chevrons to point East/South instead of South/North.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['plus_minus']['#description'] = $this->t('Replace expand/collapse chevrons with plus and minus signs.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['animate']['#description'] = $this->t('Animate the state changes of the expand/collapse icons.');

  }

  /**
   * {@inheritdoc}
   *
   * This is super cheesy, and should be replaced with theme functions and
   * templates.
   */
  public function renderGroupingSets($sets, $level = 0) {

    $this->tpw->reset();

    $this->tpw->setElements($this->options['tabpanelwidget_settings']['main_settings']['elements']);
    $this->tpw->setBehavior($this->options['tabpanelwidget_settings']['main_settings']['behavior']);
    $this->tpw->setTabStyle($this->options['tabpanelwidget_settings']['tab_settings']['tab_style']);
    $this->tpw->setTabOptions($this->options['tabpanelwidget_settings']['tab_settings']['tab_options']);
    $this->tpw->setAccordionOptions($this->options['tabpanelwidget_settings']['accordion_settings']['accordion_options']);

    $first_set = TRUE;
    foreach ($sets as $set) {
      $default = ($first_set and $this->options['first_row_default']) ? TRUE : FALSE;
      $set_content = [];
      foreach ($set['rows'] as $row) {
        $content = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'views-row',
            ],
          ],
          0 => $this->view->rowPlugin->render($row),
        ];
        $set_content[] = $content;
      }
      $this->tpw->addItem($set['group'], $set_content, $default);

      if ($first_set) {
        $first_set = FALSE;
      }
    }

    $output = $this->tpw->build();
    return $output;
  }

}
