<?php

namespace Drupal\tabpanelwidget_quicktabs\Plugin\TabRenderer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\quicktabs\Entity\QuickTabsInstance;
use Drupal\quicktabs\TabRendererBase;
use Drupal\tabpanelwidget\Tpw;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'tabpanelwidget' tab renderer.
 *
 * @TabRenderer(
 *   id = "tabpanelwidget",
 *   name = @Translation("TabPanelWidget"),
 * )
 */
class TabPanelWidget extends TabRendererBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * TabPanelWidget service.
   *
   * @var \Drupal\tabpanelwidget\Tpw
   */
  private $tpw;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
    // $this->tpw = $container->get('tabpanelwidget.tpw');
    // $this->tpw = \Drupal::service('tabpanelwidget.tpw');
    $this->tpw = new Tpw();
  }

  /**
   * {@inheritdoc}
   *
   * Why is this being ignored?
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    // $container->get('tabpanelwidget.tpw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function optionsForm(QuickTabsInstance $instance) {
    $options = !empty($instance->getOptions()['tabpanelwidget']) ? $instance->getOptions()['tabpanelwidget'] : [];
    $default_options = \Drupal::service('config.factory')->getEditable('tabpanelwidget.settings');
    $form = [];

    $form['tabpanelwidget_settings'] = [
      '#type' => 'container',
      '#title' => $this->t('TabPanelWidget settings'),
    ];

    $form['tabpanelwidget_settings']['main_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Main settings'),
      '#open' => TRUE,
    ];

    $form['tabpanelwidget_settings']['main_settings']['elements'] = [
      '#type' => 'select',
      '#title' => $this->t('Elements'),
      '#options' => [
        'h2' => $this->t('H2'),
        'h3' => $this->t('H3'),
        'h4' => $this->t('H4'),
        'h5' => $this->t('H5'),
      ],
      '#default_value' => !empty($options['tabpanelwidget_settings']['main_settings']['elements']) ? $options['tabpanelwidget_settings']['main_settings']['elements'] : $default_options->get('elements'),
      '#description' => $this->t('<p>The element used for the headers and tabs. H3 is probably usually appropriate, though you may wish to change this if the tabset will be nested under other headers.</p>'),
    ];

    $form['tabpanelwidget_settings']['main_settings']['behavior'] = [
      '#type' => 'select',
      '#title' => $this->t('Behavior'),
      '#options' => [
        'responsive' => $this->t('Responsive'),
        'tabpanel' => $this->t('Always display as tabs'),
        'accordion' => $this->t('Always display as accordion'),
      ],
      '#default_value' => !empty($options['tabpanelwidget_settings']['main_settings']['behavior']) ? $options['tabpanelwidget_settings']['main_settings']['behavior'] : $default_options->get('behavior'),
    ];

    $form['tabpanelwidget_settings']['tab_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Tab settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="options[tabpanelwidget][tabpanelwidget_settings][main_settings][behavior]"]' => [
            ['value' => 'responsive'],
            ['value' => 'tabpanel'],
          ],
        ],
      ],
    ];

    $form['tabpanelwidget_settings']['tab_settings']['tab_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Tab style'),
      '#options' => [
        'standard' => $this->t('Standard'),
        'fancy' => $this->t('Fancy'),
        'pills' => $this->t('Pills'),
        'bar' => $this->t('Bar'),
      ],
      '#default_value' => !empty($options['tabpanelwidget_settings']['tab_settings']['tab_style']) ? $options['tabpanelwidget_settings']['tab_settings']['tab_style'] : $default_options->get('tab_style'),
      '#description' => $this->t('<p><b>Standard:</b> Foreground tab is styled, background tabs are plain labels.<br><b>Fancy:</b> All tabs are styled.<br><b>Pills:</b> Tabs are displayed as a row of "pills."<br><b>Bar:</b> Tabs are plain labels with a bar highlighting the selected tab.</p>'),
    ];

    $form['tabpanelwidget_settings']['tab_settings']['tab_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Tab options'),
      '#options' => [
        'centered' => $this->t('Centered'),
        'rounded' => $this->t('Rounded'),
      ],
      '#default_value' => [],
    ];
    $defaults_array = !empty($options['tabpanelwidget_settings']['tab_settings']['tab_options']) ? $options['tabpanelwidget_settings']['tab_settings']['tab_options'] : $default_options->get('tab_options');
    foreach ($defaults_array as $key => $value) {
      if ($value) {
        $form['tabpanelwidget_settings']['tab_settings']['tab_options']['#default_value'][] = $key;
      }
    }
    $form['tabpanelwidget_settings']['tab_settings']['tab_options']['centered']['#description'] = $this->t('Tabs are centered rather than left-aligned.');
    $form['tabpanelwidget_settings']['tab_settings']['tab_options']['rounded']['#description'] = $this->t('Tabs and borders have rounded corners. Does not apply to "Bar" style.');

    $form['tabpanelwidget_settings']['accordion_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Accordion settings'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="options[tabpanelwidget][tabpanelwidget_settings][main_settings][behavior]"]' => [
            ['value' => 'responsive'],
            ['value' => 'accordion'],
          ],
        ],
      ],
    ];

    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Accordion options'),
      '#options' => [
        'disconnected' => $this->t('Disconnected'),
        'icons_at_the_end' => $this->t('Icons at the end'),
        'chevrons_east_south' => $this->t('Chevrons east/south'),
        'plus_minus' => $this->t('Replace chevrons with +/-'),
        'animate' => $this->t('Animate expand/collapse icons'),
      ],
      '#default_value' => [],
    ];
    $defaults_array = !empty($options['tabpanelwidget_settings']['accordion_settings']['accordion_options']) ? $options['tabpanelwidget_settings']['accordion_settings']['accordion_options'] : $default_options->get('accordion_options');
    foreach ($defaults_array as $key => $value) {
      if ($value) {
        $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['#default_value'][] = $key;
      }
    }
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['disconnected']['#description'] = $this->t('Creates space between the headers and styles them with rounded corners.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['icons_at_the_end']['#description'] = $this->t('Display the expand/collapse icons at the end of the headers.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['chevrons_east_south']['#description'] = $this->t('Override expand/collapse chevrons to point East/South instead of South/North.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['plus_minus']['#description'] = $this->t('Replace expand/collapse chevrons with plus and minus signs.');
    $form['tabpanelwidget_settings']['accordion_settings']['accordion_options']['animate']['#description'] = $this->t('Animate the state changes of the expand/collapse icons.');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function render(QuickTabsInstance $instance) {

    $options = $instance->getOptions()['tabpanelwidget'];
    $this->tpw->setElements($options['tabpanelwidget_settings']['main_settings']['elements']);
    $this->tpw->setBehavior($options['tabpanelwidget_settings']['main_settings']['behavior']);
    $this->tpw->setTabStyle($options['tabpanelwidget_settings']['tab_settings']['tab_style']);
    $this->tpw->setTabOptions($options['tabpanelwidget_settings']['tab_settings']['tab_options']);
    $this->tpw->setAccordionOptions($options['tabpanelwidget_settings']['accordion_settings']['accordion_options']);

    $type = \Drupal::service('plugin.manager.tab_type');

    // Finally we'll render the markup.
    foreach ($instance->getConfigurationData() as $index => $tab) {
      $object = $type->createInstance($tab['type']);
      $render = $object->render($tab);

      // If user wants to hide empty tabs and there is no content
      // then skip to next tab.
      if ($instance->getHideEmptyTabs() && empty($render)) {
        continue;
      }

      $content = [];
      if (!empty($tab['content'][$tab['type']]['options']['display_title']) && !empty($tab['content'][$tab['type']]['options']['block_title'])) {
        $content['title'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'tpw-qt-panel-title',
            ],
          ],
          'title' => [
            '#markup' => $tab['content'][$tab['type']]['options']['block_title'],
          ],
        ];
      }
      $content['panel'] = $render;

      $default = $instance->getDefaultTab() === $index ? TRUE : FALSE;
      $this->tpw->addItem($tab['title'], $content, $default);

    }

    $build = $this->tpw->build();

    // This extra library provides title styling for blocks inside of TPW body
    // content.
    $build['#attached']['library'][] = 'tabpanelwidget_quicktabs/tabpanelwidget_quicktabs.global';

    return $build;
  }

}
